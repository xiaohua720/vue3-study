import {
  createRouter,
  createWebHistory, //history
  createWebHashHistory, //hash
} from "vue-router";

import Layout from "../layout/index.vue";
//分别暴露(可以写多个)
export const routes = [
  {
    //// 路由路径
    path: "/",
    // 路由组件（路由懒加载加载组件）
    component: Layout,
    // 路由名称
    name: "home",
    meta: { title: "首页" },
    //子路由
    children: [
      {
         // 没有路径，会自动补全父路由地址
        path: "",
        // 路由组件（路由懒加载加载组件）
        component: () => import("../views/home/index.vue"),
        name: "home",
      },
    ],
  },
  {
    path: "/course",
    component: Layout,
    name: "course",
    meta: { title: "课程" },
    children: [
      {
        path: "",
        component: () => import("../views/course/index.vue"),
        name: "course",
        children: [
          {
            path: "frontend",
            component: () => import("../views/course/frontEnd/index.vue"),
            name: "Frontend",
          },
          {
            path: "backend",
            component: () => import("../views/course/backEnd/index.vue"),
            name: "Backend",
          },
        ],
      },
    ],
  },
  {
    path: "/pins",
    component: Layout,
    name: "pins",
    meta: { title: "沸点" },
    children: [
      {
        path: "",
        component: () => import("../views/pins/index.vue"),
        name: "pins",
      },
    ],
  },
  {
    path: "/:pathMatch(.*)*", //匹配任意地址 当都匹配不到时则跳转到主页面
    redirect: "/",
    isDone: true,
  },
];

const router = createRouter({
  history: createWebHistory(),

  routes,
});

//默认暴露(只能写一个)
export default router;
