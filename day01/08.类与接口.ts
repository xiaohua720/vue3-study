class Person {
  name: string;
  constructor(name: string) {
    this.name = name;
  }
}

// 接口继承类
interface People extends Person {
  age: number;
}

// const p: People = {
//   age: 18,
//   name: "jack",
// };

interface A {
  name: string;
}

// 接口继承接口
interface B extends A {
  age: number;
}

// 类继承类
class C {}
class D extends C {}

interface F {}
// 类实现接口(只能实现，不能继承)
class E implements F {}
