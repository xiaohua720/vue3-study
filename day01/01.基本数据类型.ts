const text: string = "hello world";

//基本数据类型
const a: number = 1;
const b: string = "2";
const c: boolean = true;
const d: null = null;
const e: undefined = undefined;
const f: symbol = Symbol();
