/*
 * @Author: xiaohua720 1643789444@qq.com
 * @Date: 2024-06-22 16:15:47
 * @LastEditors: xiaohua720 1643789444@qq.com
 * @LastEditTime: 2024-06-24 10:15:54
 * @FilePath: \vue3-study\day01\09.扩展类型.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 1. 元组
const arr: number[] = [1, 2, 3]; // 数组
const arr1: [number, string] = [1, "hello"]; // 元组：对数组长度和值的类型有要求
// 允许元组越界：继续往数组添加元素（元素类型是联合类型）
arr1.push(2);
arr1.push("str");
// arr1.push(true) // error

// 2. 枚举
// enum Light {
//   // 默认值从0开始
//   red,
//   green,
//   yellow,
// }

// console.log(Light.red); // 0
// console.log(Light.green); // 1
// console.log(Light.yellow); // 2

// enum Light {
//   red,
//   green = 5,
//   yellow,
// }

// console.log(Light.red); // 0
// console.log(Light.green); // 5
// console.log(Light.yellow); // 6
// console.log(Light[0]); // red
// console.log(Light[5]); // green
// console.log(Light[6]); // yellow

enum Sex {
  male = 1,
  female = 0,
}

// 3. void 常用函数没有返回值
const fn = () => {};

// 4. any 任意值(不建议使用)
let a: any = 123;
a = "hello";
a = { name: "jack" };
a.map();
a.join();
a.call();

// 5. unknown 不知道类型
let b: unknown = 123;
b = "hello";
b = { name: "jack" };
// 读写OK，但是操作属性和方法往往不行
(b as number[]).map(() => {});

// 6. never 永不存在类型
function foo(): never {
  throw new Error("error");
}

function foo1(): never {
  while (true) {}
}