// class Person {
//   // 定义属性类型
//   name: string;
//   age: number;
//   // new Person() 会自动触发constructor方法
//   // new 类传递的参数，就是constructor方法接受到的参数
//   constructor(name: string, age: number) {
//     this.name = name;
//     this.age = age;
//   }
//   setName(name: string): void {
//     this.name = name;
//   }
// }

// 定义类可以new调用，同时也能当做类型使用
// const p: Person = {
//   name: "张三",
//   age: 18,
//   setName() {},
// }

// const p = new Person("张三", 18);

//继承
// class Father {}
// class Son extends Father {}

/*
  public 公共的，公开的 -> 任意访问
  private 私有的 -> 只能在当前类的内部使用（类的外部和子类都不能使用）
  protected 受保护的 -> 只能在当前类和子类的内部使用（类和子类的外部不能使用）

  static 静态的。 -> 定义的数据给类使用（而不是实例对象）
*/
class Person {
  public name: string;
  private age: number;
  protected sex: string;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  protected setName(name: string): void {
    this.name = name;
  }

  setAge(age: number) {
    this.age = age;
  }
}

const p = new Person("jack", 18);
p.name; // ok
// p.age; // error
// p.setName("tom"); // error
// p.sex; // error
// Person.sex; // ok

class Son extends Person {
  sayAge() {
    // console.log(this.age); // error 
    // this.setName("tom"); // ok
  }
}

