// 使用 interface 定义对象类型
interface Person {
  readonly id: number; // readonly 只读的
  name: string;
  age: number;
  sex?: string;
  setName(name: string): void; // void 常用函数没有返回值（undefined 或 null）
  // 任意属性
  // [key: string]: any;
}
const person: Person = {
  id: 1,
  name: "jack",
  age: 18,
  setName(name) {
    this.name = name;
  },
    // aaa: "", // 多写属性不行
};

interface IAge {
  name: string;
}

interface IAge {
  age: number;
}

const ages: IAge = {
  name: "dog",
  age: 1,
};


// 定义函数类型
interface Fn {
  (x: number, y: number): number;
}

const fn1: Fn = (x, y) => x + y;
const fn2: Fn = (x, y) => x + y;

//定义数组类型
interface Arr {
  [index: number]: string;
}

const arr: Arr = ["hello"];
