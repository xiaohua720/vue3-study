function reverse(value: string): string;
function reverse(value: number): number;
function reverse(value: string | number): string | number {
  if (typeof value === "string") {
    return value.split("").reverse().join("");
  } else {
    return Number(String(value).split("").reverse().join(""));
  }
}
const res = reverse(1234);
