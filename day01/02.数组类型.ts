//方括号写法
const arr1: number[] = [1, 2, 3];
const arr2: string[] = ["1", "2", "3"];
//尖括号写法
const arr3: Array<number> = [1, 2, 3];
