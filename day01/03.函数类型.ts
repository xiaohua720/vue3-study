// 函数声明
function fn1(x: number, y: number): number {
  return x + y;
}
// 函数表达式
const fn2 = function (x: number, y: number): number {
  return x + y;
};

const fn3 = (x: number, y: number): number => {
  return x + y;
};
const fn4: (x: number, y: number) => number = function (x, y) {
  return x + y;
};

// 完整写法
const fn5: (x: number, y: number) => number = function (
  x: number,
  y: number
): number {
  return x + y;
};

// 直接写的参数：是必填参数
// 加上 ? 可选参数
// 加上默认值 可选参数
const fn6 = function (firstName?: string, lastName: string = "a") {
  console.log(firstName + lastName);
};
fn6("张三");

// ...args: number[] 剩下所有参数
const fn7 = (a: number, ...args: number[]) => {};


// this -> { name: 'jack', age: 18 }
// 如果要给函数的this指定类型，必须写在第一个参数 this: 类型  （不占用参数名额）
function Person(this: IPerson,name: string, age: number) {
  this.name = name;
  this.age = age;
}
const p = new Person("jack", 18);

interface IPerson {
  name: string;
  age: number;
}
