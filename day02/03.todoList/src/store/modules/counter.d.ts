export interface Todos {
  id: number;
  name: string;
  isDone: boolean;
}

export interface CounterStore {
  todos: Todos[];
}
