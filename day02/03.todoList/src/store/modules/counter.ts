import { defineStore } from "pinia";

import type { CounterStore } from "./counter.d";
const useTodoStore = defineStore("counter", {
  state: (): CounterStore => {
    return {
      todos: [
        { id: 1, name: "吃饭", isDone: false },
        { id: 2, name: "睡觉", isDone: false },
        { id: 3, name: "敲代码", isDone: true },
      ],
    };
  },
  getters: {
    total(state) {
      return state.todos.length;
    },
    finished(state) {
      return state.todos.reduce((p, c) => {
        if (c.isDone) {
          p++;
        }
        return p;
      }, 0);
    },
  },
  actions: {
    addTodo(name: string) {
      this.todos.push({
        id: Date.now(),
        name,
        isDone: false,
      });
    },
    updateTodoIsDone(id: number) {
      const todo: any = this.todos.find((todo) => todo.id === id);
      todo.isDone = !todo.isDone;
    },
    dltTodo(index: number) {
      this.todos.splice(index, 1);
    },
    allTodoIsDone(isDone: boolean) {
      this.todos.forEach((todo) => {
        todo.isDone = isDone;
      });
    },
    clearAll() {
      this.todos = this.todos.filter((todo) => !todo.isDone);
    },
  },
});

export default useTodoStore;
