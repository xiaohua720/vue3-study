import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", {
  state: () => {
    return {
      count: 0,
    };
  },
  getters: {
    oddOrEven(state) {
      return state.count % 2 === 0 ? "偶数" : "奇数";
    }
  },
  actions: {
    addCount() {
      this.count++;
    },
    minsCount() {
      this.count--;
    }
  },
});
